#!/bin/bash

#SBATCH -A mdm_db_coli
#SBATCH -p long
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=40
#SBATCH -J iqt_sir2-sir2-like  # fill job name
#SBATCH -o log_iqtree.out
#SBATCH -e log_iqtree.err
#SBATCH --mail-type=ALL
#SBATCH --mail-user=hvaysset@pasteur.fr

afa_file="data/sirtuins_sir2-like_kingdom=all_cured_gappy=0.99_no-dupl.clipkit"  # fill_here
n_iter_max=1000
model="Q.pfam+G4"  # "TEST" for model selection
n_cpus=${SLURM_CPUS_PER_TASK}  # "AUTO" for automatic selection
afa=$(basename ${afa_file})

touch .RUNNING

iqtree --nmax ${n_iter_max} -s ${afa_file} -st AA -B 1000 -m ${model} -nt ${n_cpus} -pre results/${afa%.*clipkit}  # Make sure that extension is clipkit!  

mv .RUNNING .TO_ANALYZE

#!/bin/bash

#SBATCH -A mdm_db_coli
#SBATCH -p fast
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=20
#SBATCH -J muscle  # fill job name
#SBATCH -o log_muscle.out
#SBATCH -e log_muscle.err
#SBATCH --mail-type=ALL
#SBATCH --mail-user=hvaysset@pasteur.fr

input_faa="data/sir2-like_with-eukprot_with-pf14253_kingdom=all_hit_region_rep_seq_root=sirtuins_cured.faa"
faa_file=$(basename ${input_faa})
align_afa="results/${faa_file%.faa}.muscle_@.afa"

muscle -threads 20 -perturb 0 -perm all -super5 ${input_faa} -output ${align_afa}


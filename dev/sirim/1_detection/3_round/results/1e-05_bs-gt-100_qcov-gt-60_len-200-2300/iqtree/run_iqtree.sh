#!/bin/bash

#SBATCH -A mdm_db_coli
#SBATCH -p long
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=10
#SBATCH -J iqt_sir2-like_euk
#SBATCH -o log_iqtree.out
#SBATCH -e log_iqtree.err
#SBATCH --mail-type=ALL
#SBATCH --mail-user=hvaysset@pasteur.fr

touch .RUNNING

iqtree --undo --nmax 3000 -s data/fam118b_round-3_kingdom=euk_1e-05_bs-gt-100_qcov-gt-60_len-200-2300_95-95_2406-3009.gappy=99.clipkit -st AA -B 1000 -m TEST -nt ${SLURM_CPUS_PER_TASK} -pre results/fam118b_round-3_kingdom=euk_1e-05_bs-gt-100_qcov-gt-60_len-200-2300_95-95_2406-3009.gappy=99.treefile

mv .RUNNING .TO_ANALYZE

#!/bin/bash

#SBATCH -A mdm_db_coli
#SBATCH -p long
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=40
#SBATCH -J iqt_sir2-like_euk_with_sirt
#SBATCH -o log_iqtree.out
#SBATCH -e log_iqtree.err
#SBATCH --mail-type=ALL
#SBATCH --mail-user=hvaysset@pasteur.fr

if [ -f .TO_ANALYZE]; then
  rm .TO_ANALYZE;
fi
touch .RUNNING

iqtree --nmax 5000 -s data/fam118b_round-3_98-98_rep_seq_root=sirtuins_3865-6146_sg.clipkit -st AA -B 1000 -m TEST -nt ${SLURM_CPUS_PER_TASK} -pre results/fam118b_round-3_98-98_rep_seq_root=sirtuins_sir2-domain

mv .RUNNING .TO_ANALYZE

# Goal
Identify which Eukprot hits could be bacterial contaminations.

# Results
- Diamond (sensitive mode, v2.1) of all Eukprot hits against NR (stored in /shared/bank/nr/current/diamond/nr) 
- We keep the 25 best hits per query sequence (n=10,220 hits in total), and analyze all the hits with %identity > 70% (n=1,122)
- Those hits are fetched using their NCBI accession (command `efetch -db protein -id $ACCESSION -format tsv | grep -A 1 "ORGANISM" | sed -z 's/\n/\t/g'`)
- We identify n=31 Eukprot SIR2-like hits which share > 70% (often > 95% identity) with bacterial proteins in NR. Those hits correspond to isolated sequences in the SIR2-like tree.
-> They are bacterial contaminations
-> As mentioned in Culberston & Levin (2023), Eukaryotic clades should be considered valid (e.g. due to HGT) only if they contain more than 1 sequence (in their case it is even more strict : 2 species are required to consider a clade valid). More generally their criteria are :
    (1) Eukaryotic and bacterial sequences branched adjacent to one another with strong support (bootstrap values >70)
    (2) The eukaryotic sequences formed a distinct subclade, represented by at least 2 species from the same eukaryotic supergroup
    (3) The eukaryotic sequences were produced by at least 2 different studies
    (4) The position of the horizontally transferred sequences was robust across all alignment and phylogenetic reconstruction methods used

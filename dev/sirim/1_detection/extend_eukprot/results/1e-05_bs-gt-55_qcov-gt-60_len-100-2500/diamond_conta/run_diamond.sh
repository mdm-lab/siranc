#!/bin/bash

#SBATCH -A mdm_db_coli
#SBATCH -p long
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=40
#SBATCH -J diamond_eukprot  # fill job name
#SBATCH -o log_diamond.out
#SBATCH -e log_diamond.err
#SBATCH --mail-type=ALL
#SBATCH --mail-user=hvaysset@pasteur.fr

diamond blastp -v --threads $SLURM_CPUS_PER_TASK --fast -d /shared/bank/nr/current/diamond/nr -q data/fam118b_eukprot_kingdom=all_1e-05_bs-gt-55_qcov-gt-60_len-100-2500_hits.faa -o results/fam118b_hits_diamond.tsv --outfmt 6

#! /bin/bash

#SBATCH -A mdm_db_coli
#SBATCH -J reannot_pfam_6
#SBATCH -N 1 # number of nodes required
#SBATCH --ntasks=1
#SBATCH --mem 32Gb
#SBATCH --cpus-per-task=40
#SBATCH -o log_reannot_pfam.out
#SBATCH -e log_reannot_pfam.err

# Parameters
faa_file="data/fam118b_round-7-adl_kingdom=all_1e-05_bs-gt-20_qcov-gt-60_len-200-2300_hits.faa"  # Fill here
dn=$(dirname ${faa_file})
fn=$(basename ${faa_file})
out_file=${fn%.faa}

echo "Running on ${SLURM_CPUS_PER_TASK} cpus."

echo "Running hmmsearch..."
hmmsearch --cut_ga --cpu $SLURM_CPUS_PER_TASK --tblout results/${out_file}_tblout.txt --domtblout results/${out_file}_domtblout.txt --pfamtblout results/${out_file}_pfam_pftblout.txt /shared/ifbstor1/projects/mdm_db_computations/GENOMES_DATABASE/Pfam-A/Pfam-A.hmm data/${fn};

cat results/${out_file}_tblout.txt | awk '$5 < 1e-5 {print $3}' | sort | uniq -c | sort -g > results/${out_file}_tblout.counts;

echo "Done!"




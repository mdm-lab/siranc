#! /bin/bash

#SBATCH -A mdm_db_coli
#SBATCH -J reannot_pfam
#SBATCH -N 1 # number of nodes required
#SBATCH --ntasks=1
#SBATCH --mem 32Gb
#SBATCH --cpus-per-task=20
#SBATCH -o log_reannot_pfam.out
#SBATCH -e log_reannot_pfam.err

# Parameters
faa_file="data/fam118b_round-4_kingdom=euk_1e-05_bs-gt-100_qcov-gt-60_len-200-2300_hits.faa"
fn=$(basename faa_file)
out_file=${fn%.faa}

echo "Running on ${SLURM_CPUS_PER_TASK} cpus."

echo "Running hmmsearch..."
hmmsearch --cpu $SLURM_CPUS_PER_TASK --tblout results/${out_name}_tblout.txt --domtblout results/${out_name}_domtblout.txt --pfamtblout results/${out_name}_pfam_pftblout.txt /shared/ifbstor1/projects/mdm_db_computations/GENOMES_DATABASE/Pfam-A/Pfam-A.hmm ${faa_file};

cat results/${out_name}_tblout.txt | awk '$5 < 1e-5 {print $3}' | sort | uniq -c | sort -g > results/${out_name}_tblout.counts;

echo "Done!"




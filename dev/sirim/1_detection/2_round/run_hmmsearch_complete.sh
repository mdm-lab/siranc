#!/bin/bash

#SBATCH -A mdm_db_coli
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=10
#SBATCH -J hmms_fam_round-2
#SBATCH -o log_hmmsearch.out
#SBATCH -e log_hmmsearch.err

echo "Using ${SLURM_CPUS_PER_TASK} cores."

file_name="fam118b_round-2"
hmm_profile="data/fam118b_round-1_hits_1e-5_len-200-600_kingdom=euk_845-1486.hmm"
max_e_val="1e-5"
min_bit_score=10
min_q_cov=80  # min query (profile HMM) coverage
min_len=200
max_len=1500
mmseqs_id=0.90
mmseqs_cov=0.90

mkdir results/raw results/reports results/mmseqs results/align results/annot

# hmmsearch
hmmsearch --cpu $SLURM_CPUS_PER_TASK --tblout results/${file_name}_kingdom=euk_tblout.txt --domtblout results/${file_name}_kingdom=euk_domtblout.txt --pfamtblout results/${file_name}_kingdom=euk_pftblout.tsv ${hmm_profile} /shared/ifbstor1/projects/mdm_db_computations/GENOMES_DATABASE/Eukaryotes/Eukaryote_Genbank/Genomes/ALL_EUK_PROT.prot
# Filter hits based on provided conditions
python ~/scripts/hmmer/filter_hmmsearch_hits.py --tblout results/${file_name}_kingdom=euk_tblout.txt --domtblout results/${file_name}_kingdom=euk_domtblout.txt --e_value ${max_e_val} --bit_score ${min_bit_score} --q_cov ${min_q_cov} --min_len ${min_len} --max_len ${max_len}
fmt_tblout_file=$(cat results/reports/*.report | tail -n 1 | awk -F':' '{print $2}' | sed 's/\n//g')  # get name of formatted files
fmt_domtblout_file=$(echo ${fmt_tblout_file%_tblout.tsv}_domtblout.tsv)  # get name of formatted files
hit_ids_file=${fmt_tblout_file%_tblout.tsv}_hits.ids
cat results/${fmt_tblout_file} | grep -v "target_accession" | awk '{print $1}' > results/${hit_ids_file}

# Retrieve proteins
getprot-para2 -c $SLURM_CPUS_PER_TASK results/${hit_ids_file}  # get euk proteins
hit_faa_file=${hit_ids_file%.ids}.faa 
mv ${hit_ids_file}.faa results/${hit_faa_file}

# Extract only the hit region
# Generate domtbl with hit coordinates and coverage
# min_q_cov_str=$(awk -v x=${min_q_cov} '{print x * 100}' "")
# cat results/${file_name}_kingdom=euk_domtblout.txt| awk 'BEGIN {print "#target\ttarget_begin\ttarget_end\thit_len\tquery_cov\ttarget_cov"}{print $1"\t"$20"\t"$21"\t"($21 - $20)"\t"($21 - $20)/$6"\t"($21 - $20)/$3}' > results/${file_name}_kingdom=euk_coord.tsv
# while read ID s e l qc tc; do seqkit grep -p $ID results/${file_name}_kingdom=euk_${max_e_val}_len-${min_len}-${max_len}_hits.faa | seqkit subseq -r $s:$e -R; done < <(cat results/${file_name}_kingdom=euk_coord.tsv | grep -v "#" | awk -v min_q_coverage="$min_q_cov_str" '$5 > min_q_coverage') > results/${file_name}_kingdom=euk_${max_e_val}_len-${min_len}-${max_len}_qcov-gt-${min_q_cov}_hit-region.faa  # extract only the region hit by the profile

# n_seqs=$(seqkit stat results/${file_name}_kingdom=euk_${max_e_val}_hits.faa)
# n_seqs_qcov=$(seqkit stat results/${file_name}_kingdom=euk_${max_e_val}_qcov-gt-${min_q_cov}_hit-region.faa)
# echo "$n_seqs sequences found in the database." >> results/${file_name}_kingdom=euk_hits.report
# seqkit fx2tab --length --name results/${file_name}_kingdom=euk_${max_e_val}_hits.faa | awk '{print $1"\t"$NF}' > results/${file_name}_kingdom=euk_${max_e_val}_hits_len.tsv
# cat results/${file_name}_hits_${max_e_val}_kingdom=euk_len.tsv | awk '{print $2}' | hist -x -b 100 > results/${file_name}_kingdom=euk_${max_e_val}_hits_len.hist 
# echo "$n_seqs_len sequences with ${min_len} < length < ${max_len}." >> results/${file_name}_kingdom=euk_hits.report

# mmseqs
mmseqs_id_str=$(awk -v x=${mmseqs_id} '{print x * 100}' "")
mmseqs_cov_str=$(awk -v x=${mmseqs_cov} '{print x * 100}' "")
mmseqs_out_file=$(echo "${hit_faa_file%_hits.faa}_${mmseqs_id}-${mmseqs_cov}")
mmseqs easy-cluster results/${hit_faa_file} results/mmseqs/${mmseqs_out_file} tmp -c ${mmseqs_cov} --min-seq-id ${mmseqs_id} --threads $SLURM_CPUS_PER_TASK --cov-mode 0

# Align extracted regions
mafft --thread $SLURM_CPUS_PER_TASK --reorder --localpair results/mmseqs/${mmseqs_out_file}_rep_seq.fasta > results/align/${mmseqs_out_file}_rep_seq.mafft.afa
muscle --threads $SLURM_CPUS_PER_TASK -super5 results/mmseqs/${mmseqs_out_file}_rep_seq.fasta -output results/align/${mmseqs_out_file}_rep_seq.muscle.afa

# Move raw to raw and report files (histograms, report etc.) to reports
mv results/${file_name}_kingdom=euk_tblout.txt results/${file_name}_kingdom=euk_domtblout.txt results/${file_name}_kingdom=euk_pftblout.txt results/raw


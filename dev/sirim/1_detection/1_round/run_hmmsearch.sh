#! /bin/bash

#SBATCH -A mdm_db_computations
#SBATCH -J run_hmmsearch
#SBATCH -N 1 # number of nodes required
#SBATCH --ntasks=1
#SBATCH --mem 64Gb
#SBATCH --cpus-per-task=40
#SBATCH -o log_hmmsearch.out
#SBATCH -e log_hmmsearch.err

# coli_base environment already activated
# hmmsearch version 3.3.2
hmmsearch --cpu $SLURM_CPUS_PER_TASK --tblout results/fam118b_round-1_hits.tsv --domtblout results/fam118b_round-1_domtbl.tsv --pfamtblout results/fam118b_round-1_pftblout.tsv data/FAM118B_orthologs_cured.hmm /shared/ifbstor1/projects/mdm_db_computations/GENOMES_DATABASE/Eukaryotes/Eukaryote_Genbank/Genomes/ALL_EUK_PROT.prot


#!/bin/bash

#SBATCH -A mdm_db_coli
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=10
#SBATCH -J hmms_fam_round-1
#SBATCH -o log_hmmsearch.out
#SBATCH -e log_hmmsearch.err

file_name="fam118b_round-1"
hmm_profile="data/FAM118B_orthologs_cured_171-790.hmm"
min_len=200
max_len=700
min_e_val="1e-5"
min_q_cov="0.80"  # min query (profile HMM) coverage)
mmseqs_id="0.90"
mmseqs_cov="0.90"

mkdir results/raw results/reports

# hmmsearch
# hmmsearch --cpu $SLURM_CPUS_PER_TASK --tblout results/${file_name}_hits_kingdom=euk.tsv --domtblout results/${file_name}_domtbl_kingdom=euk.tsv --pfamtblout results/${file_name}_pftblout_kingdom=euk.tsv ${hmm_profile} /shared/ifbstor1/projects/mdm_db_computations/GENOMES_DATABASE/Eukaryotes/Eukaryote_Genbank/Genomes/ALL_EUK_PROT.prot

# Concatenate hits table
cat results/${file_name}_hits_kingdom=euk.tsv | grep -v "#" | awk -v min_e=${min_e_val} '$5 < min_e' >  results/${file_name}_hits_${min_e_val}_kingdom=euk.tsv
cat results/${file_name}_hits_${min_e_val}_kingdom=euk.tsv | awk '{print $1}' > results/${file_name}_hits_${min_e_val}_kingdom=euk.ids

# Concatenate domtbl
cat results/${file_name}_domtbl_kingdom=euk.tsv| awk 'BEGIN {print "#target\ttarget_begin\ttarget_end\thit_len\tquery_cov\ttarget_cov"}{print $1"\t"$20"\t"$21"\t"($21 - $20)"\t"($21 - $20)/$6"\t"($21 - $20)/$3}' > results/${file_name}_coord_kingdom=euk.tsv

# Generate histograms
cat results/${file_name}_hits_${min_e_val}_kingdom=euk.tsv | awk '{print $5}' | awk -F'e' '{if (NF > 1) {print $2}}' | hist -x -b 100 > results/${file_name}_hits_${min_e_val}_kingdom=euk_e-value.hist
cat results/${file_name}_hits_${min_e_val}_kingdom=euk.tsv | awk '{print $6}' | hist -x -b 100 > results/${file_name}_hits_${min_e_val}_kingdom=euk_bitscore.hist

n_bact=$(cat results/${file_name}_hits_kingdom=euk.tsv | grep -v "#" | grep -v -E "GCA|GCF" | awk  -v min_e=${min_e_val} '$5 < min_e' | wc -l)
n_euk=$(cat results/${file_name}_hits_kingdom=euk.tsv | grep -v "#" | grep -E "GCA|GCF" | awk  -v min_e=${min_e_val} '$5 < min_e {print $1}' | awk -F'_' 'NF != 3' | wc -l)
n_hum=$(cat results/${file_name}_hits_kingdom=euk.tsv | grep -v "#" | grep -E "GCA|GCF" | awk  -v min_e=${min_e_val} '$5 < min_e {print $1}' | grep "Hum" | wc -l)
n_ani=$(cat results/${file_name}_hits_kingdom=euk.tsv | grep -v "#" | grep -E "GCA|GCF" | awk  -v min_e=${min_e_val} '$5 < min_e {print $1}' | grep "Ani" | wc -l)
n_fun=$(cat results/${file_name}_hits_kingdom=euk.tsv | grep -v "#" | grep -E "GCA|GCF" | awk  -v min_e=${min_e_val} '$5 < min_e {print $1}' | grep "Fun" | wc -l)
n_pla=$(cat results/${file_name}_hits_kingdom=euk.tsv | grep -v "#" | grep -E "GCA|GCF" | awk  -v min_e=${min_e_val} '$5 < min_e {print $1}' | grep "Pla" | wc -l)
n_pro=$(cat results/${file_name}_hits_kingdom=euk.tsv | grep -v "#" | grep -E "GCA|GCF" | awk  -v min_e=${min_e_val} '$5 < min_e {print $1}' | grep "Pro" | wc -l)
n_arch=$(cat results/${file_name}_hits_kingdom=euk.tsv | grep -v "#" | awk  -v min_e=${min_e_val} '$5 < min_e {print $1}' | awk -F'_' 'NF == 3' | wc -l)
n=$(cat results/${file_name}_hits_${min_e_val}_kingdom=euk.ids | wc -l)
echo "Found $n hits with e-value < ${min_e_val}." >> results/${file_name}_hits_kingdom=euk.report
echo "- $n_bact in Bacteria." >> results/${file_name}_hits_kingdom=euk.report
echo "- $n_arch in Archaea." >> results/${file_name}_hits_kingdom=euk.report
echo "- $n_euk in Eukaryotes." >> results/${file_name}_hits_kingdom=euk.report
echo "- $n_hum in Humans." >> results/${file_name}_hits_kingdom=euk.report
echo "- $n_ani in Animals." >> results/${file_name}_hits_kingdom=euk.report
echo "- $n_fun in Fungi." >> results/${file_name}_hits_kingdom=euk.report
echo "- $n_pro in Protozoa." >> results/${file_name}_hits_kingdom=euk.report

# Retrieve proteins
getprot-para2 -c $SLURM_CPUS_PER_TASK results/${file_name}_hits_${min_e_val}_kingdom=euk.ids  # get euk proteins
mv ${file_name}_hits_${min_e_val}_kingdom=euk.ids.faa results/${file_name}_hits_${min_e_val}_kingdom=euk.faa 
seqkit seq -m ${min_len} -M ${max_len} results/${file_name}_hits_${min_e_val}_kingdom=euk.faa > results/${file_name}_hits_${min_e_val}_len-${min_len}-${max_len}_kingdom=euk.faa
n_seqs_len=$(seqkit stat results/${file_name}_hits_${min_e_val}_len-${min_len}-${max_len}_kingdom=euk.faa)  # get proteins of sufficient size

# Extract only the hit region
while read ID s e l qc tc; do seqkit grep -p $ID results/${file_name}_hits_${min_e_val}_len-${min_len}-${max_len}_kingdom=euk.faa | seqkit subseq -r $s:$e -R; done < <(cat results/${file_name}_coord_kingdom=euk.tsv | grep -v "#" | awk -v min_q_coverage="$min_q_cov" '$5 > min_q_coverage') > results/${file_name}_hit-region_${min_e_val}_len-${min_len}-${max_len}_qcov-gt-${min_q_cov}_kingdom=euk.faa  # extract only the region hit by the profile

n_seqs=$(seqkit stat results/${file_name}_hits_${min_e_val}_kingdom=euk.faa)
n_seqs_qcov=$(seqkit stat results/${file_name}_hit-region_${min_e_val}_qcov-gt-${min_q_cov}_kingdom=euk.faa)
echo "$n_seqs sequences found in the database." >> results/${file_name}_hits_kingdom=euk.report
seqkit fx2tab --length --name results/${file_name}_hits_${min_e_val}_kingdom=euk.faa | awk '{print $1"\t"$NF}' > results/${file_name}_hits_${min_e_val}_kingdom=euk_len.tsv
cat results/${file_name}_hits_${min_e_val}_kingdom=euk_len.tsv | awk '{print $2}' | hist -x -b 100 > results/${file_name}_hits_${min_e_val}_kingdom=euk_len.hist 
echo "$n_seqs_len sequences with ${min_len} < length < ${max_len}." >> results/${file_name}_hits_kingdom=euk.report

# mmseqs
mmseqs_id_str=$(awk -v x=${mmseqs_id} '{print x * 100}' "")
mmseqs_cov_str=$(awk -v x=${mmseqs_cov} '{print x * 100}' "")
mmseqs easy-cluster results/${file_name}_hit-region_${min_e_val}_len-${min_len}-${max_len}_qcov-gt-${min_q_cov}_kingdom=euk.faa results/${file_name}_hit-region_${mmseqs_id_str}-${mmseqs_cov_str}_kingdom=euk tmp -c ${mmseqs_cov} --min-seq-id ${mmseqs_id} --threads 20 --cov-mode 0

# Align extracted regions
mafft --thread $SLURM_CPUS_PER_TASK --reorder --localpair results/${file_name}_hit-region_${mmseqs_id_str}-${mmseqs_cov_str}_kingdom=euk_rep_seq.fasta > results/${file_name}_hit-region_${mmseqs_id_str}-${mmseqs_cov_str}_kingdom=euk_rep_seq.afa

echo "Done!" >> results/${file_name}_hits_kingdom=euk.report

# Move raw to raw and report files (histograms, report etc.) to reports
mv results/*.hist results/*len.tsv results/*.report results/reports


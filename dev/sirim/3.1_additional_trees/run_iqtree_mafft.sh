#!/bin/bash

#SBATCH -A mdm_db_coli
#SBATCH -p long
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=20
#SBATCH -J iqt_mafft  # fill job name
#SBATCH -o log_iqtree_mafft.out
#SBATCH -e log_iqtree_mafft.err
#SBATCH --mail-type=ALL
#SBATCH --mail-user=hvaysset@pasteur.fr

afa_file="data/sir2-like_with-eukprot_with-pf14253_kingdom=all_hit_region_rep_seq_root=sirtuins_cured.mafft.gappy=0.99.clipkit"  # fill_here
n_iter_max=3000
model="Q.pfam+G4"  # "TEST" for model selection
n_cpus=${SLURM_CPUS_PER_TASK}  # "AUTO" for automatic selection
afa=$(basename ${afa_file})

touch .RUNNING_MAFFT

iqtree --undo --nmax ${n_iter_max} -s ${afa_file} -st AA -B 1000 -m ${model} -nt ${n_cpus} -pre results/mafft_local_iqtree_clipkit=99/${afa%.*clipkit}  # Make sure that extension is clipkit

mv .RUNNING_MAFFT .TO_ANALYZE_MAFFT

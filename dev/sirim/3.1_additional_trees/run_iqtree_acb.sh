#!/bin/bash

#SBATCH -A prokeuk_immunity
#SBATCH -p long
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=40
#SBATCH -J iqt_acb  # fill job name
#SBATCH -o log_iqtree_acb.out
#SBATCH -e log_iqtree_acb.err
#SBATCH --mail-type=ALL
#SBATCH --mail-user=hvaysset@pasteur.fr

permut="acb"
afa_file="data/sir2-like_with-eukprot_with-pf14253_kingdom=all_hit_region_rep_seq_root=sirtuins_cured.muscle_${permut}.0.gappy=0.99.clipkit"  # fill_here
n_iter_max=500
model="Q.pfam+G4"  # "TEST" for model selection
n_cpus=${SLURM_CPUS_PER_TASK}  # "AUTO" for automatic selection
afa=$(basename ${afa_file})

touch .RUNNING_${permut}

iqtree --undo --nmax ${n_iter_max} -s ${afa_file} -st AA -B 1000 -m ${model} -nt ${n_cpus} -pre results/muscle_${permut}_iqtree_clipkit=99/${afa%.*clipkit}  # Make sure that extension is clipkit

mv .RUNNING_${permut} .TO_ANALYZE_${permut}

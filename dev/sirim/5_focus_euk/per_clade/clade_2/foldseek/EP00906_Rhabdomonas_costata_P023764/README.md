We foldseek the protein from Rhabdomonas.

We hit a Zea mays protein at the N-terminal domain (A0A1D6IE20). This protein is a Histidine kinase and the specific domain which hits two
domains : PS50110 == Response regulatory domain profile (Prosite) and IPR011006 == CheY-like superfamily (Interpro).
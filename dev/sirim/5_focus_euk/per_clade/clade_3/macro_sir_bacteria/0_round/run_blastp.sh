#! /bin/bash

#SBATCH -A mdm_db_coli
#SBATCH -J blastp_macro-sir
#SBATCH -N 1 # number of nodes required
#SBATCH --ntasks=1
#SBATCH --mem 32Gb
#SBATCH --cpus-per-task=20
#SBATCH -o log_blastp.out
#SBATCH -e log_blastp.err

# Parameters
faa_file="data/"  # Fill here
dn=$(dirname ${faa_file})
fn=$(basename ${faa_file})
out_file=${fn%.faa}

blastp -query data/X8FA64.faa -db /shared/ifbstor1/projects/mdm_db_computations/GENOMES_DATABASE/Bacteria/gembase/Microbial_0722/Genomes/blast_db/bacteria_0722_blast_db -out X8FA64.res -outfmt 6

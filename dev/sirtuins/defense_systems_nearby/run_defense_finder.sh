#!/bin/bash

#SBATCH -A mdm_db_coli
#SBATCH -p long
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=50
#SBATCH -J DF_sirtuins  # fill job name
#SBATCH -o log_df.out
#SBATCH -e log_df.err
#SBATCH --mail-type=ALL
#SBATCH --mail-user=hvaysset@pasteur.fr

defense-finder run -w $SLURM_CPUS_PER_TASK data/sirtuins_fam118b_kingdom=bact-arch_context.faa --models-dir /shared/ifbstor1/projects/mdm_db_computations/MDM_SOFTS/DefenseFinder/DefenseFinder_v1.2.3/ -o results

#!/bin/bash

#SBATCH -A mdm_db_coli
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=40
#SBATCH -J hmms_sirtuins_round-6   # Fill here
#SBATCH -o log_hmmsearch.out
#SBATCH -e log_hmmsearch.err

echo "Using ${SLURM_CPUS_PER_TASK} cores."

# parameters
file_name=""
hmm_profile="data/sirtuins_round-5_kingdom=all_1e-05_bs-gt-80_qcov-gt-60_len-100-2500_60-80_rep_seq_4331-60088.hmm"
max_e_val=1e-05
min_bit_score=80
min_q_cov=60  # min query (profile HMM) coverage
min_len=100
max_len=2500
mmseqs_id=0.60
mmseqs_cov=0.80

mkdir results/reports results/mmseqs results/align results/reannotate_with_pfam 

# hmmsearch
echo "Running hmmsearch..."
hmmsearch --cpu $SLURM_CPUS_PER_TASK --tblout results/${file_name}_kingdom=all_tblout.txt --domtblout results/${file_name}_kingdom=all_domtblout.txt --pfamtblout results/${file_name}_kingdom=all_pftblout.tsv ${hmm_profile} /shared/ifbstor1/projects/mdm_db_computations/GENOMES_DATABASE/ALL_EUK_BACT-0722_ARCH.prot
echo "Done hmmsearching!"

# Filter hits based on provided conditions
python ~/scripts/hmmer/filter_hmmsearch_hits.py --tblout results/${file_name}_kingdom=all_tblout.txt --domtblout results/${file_name}_kingdom=all_domtblout.txt --e_value ${max_e_val} --bit_score ${min_bit_score} --q_cov ${min_q_cov} --min_len ${min_len} --max_len ${max_len} --draw_reports
fmt_tblout_file=$(cat results/reports/*.report | tail -n 1 | awk -F':' '{print $NF}')
fmt_domtblout_file=$(echo ${fmt_tblout_file%_tblout.tsv}_domtblout.tsv)  # get name of formatted files
hit_ids_file=${fmt_tblout_file%_tblout.tsv}_hits.ids
echo ${fmt_tblout_file}
echo ${hit_ids_file}
cat results/${fmt_tblout_file} | grep -v "target_accession" | awk '{print $1}' > results/${hit_ids_file}  # build ids file

# extract sequences
echo "Extracting sequences..."
getprot-para2 -c ${SLURM_CPUS_PER_TASK} results/${hit_ids_file}
hit_faa_file="${hit_ids_file%.ids}.faa"
echo ${hit_ids_file}
echo ${hit_faa_file}
mv ${hit_ids_file}.faa results/${hit_faa_file}
echo "Done extracting!"

# mmseqs
echo "Clustering sequence @${mmseqs_id}/${mmseqs_cov}..."
mmseqs_id_str=$(awk -v x=${mmseqs_id} '{print x * 100}' "")
mmseqs_cov_str=$(awk -v x=${mmseqs_cov} '{print x * 100}' "")
mmseqs_out_file=$(echo "${hit_faa_file%_hits.faa}_${mmseqs_id}-${mmseqs_cov}")
mmseqs easy-cluster results/${hit_faa_file} results/mmseqs/${mmseqs_out_file} tmp -c ${mmseqs_cov} --min-seq-id ${mmseqs_id} --threads $SLURM_CPUS_PER_TASK --cov-mode 0
seqkit grep -r -p "Hum" results/${hit_faa_file} >> results/mmseqs/${mmseqs_out_file}_rep_seq.fasta  # Add human sequences to representatives
echo "Done clustering!"

# Align extracted regions
echo "Aligning sequences..."
# mafft --thread $SLURM_CPUS_PER_TASK --reorder --localpair results/mmseqs/${mmseqs_out_file}_rep_seq.fasta > results/align/${mmseqs_out_file}_rep_seq.mafft.afa
muscle --threads $SLURM_CPUS_PER_TASK -super5 results/mmseqs/${mmseqs_out_file}_rep_seq.fasta -output results/align/${mmseqs_out_file}_rep_seq.muscle.afa
echo "Done aligning!"

# Reannotate with pfam
echo "Reannotating with Pfam..."
hmmsearch --cut_ga --cpu ${SLURM_CPUS_PER_TASK} --tblout results/reannotate_with_pfam/${hit_faa_file%.faa}_tblout.txt --domtblout results/reannotate_with_pfam/${hit_faa_file%.faa}_domtblout.txt --pfamtblout results/reannotate_with_pfam/${hit_faa_file%.faa}_pfam_pftblout.txt /shared/ifbstor1/projects/mdm_db_computations/GENOMES_DATABASE/Pfam-A/Pfam-A.hmm results/${hit_faa_file}
cat results/reannotate_with_pfam/${hit_faa_file%.faa}_tblout.txt | grep -v "#" | awk '$5 < 1e-03 {print $3,$4}' | sort | uniq -c | sort -g > results/reannotate_with_pfam/${hit_faa_file%.faa}_tblout.counts
echo "Done reannotating!"

# Move filtering results to a dedicated directory
filter_dir_name="${max_e_val}_bs-gt-${min_bit_score}_qcov-gt-${min_q_cov}_len-${min_len}-${max_len}"
mkdir results/${filter_dir_name}
mv results/reports results/mmseqs results/align results/annot results/reannotate_with_pfam results/*.{tsv,faa,ids} results/${filter_dir_name}

echo "Done!"

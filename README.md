# SIRanc

Phylogenetic analyses related with the evolutionary history of SIRanc (FAM118B) and SIRim proteins.

Bonhomme, Vaysset, Ednacot, Rodrigues, Cury, Hernandez Trejo, Benaroch, Morehouse, Bernheim & Poirier (2024).

### Directory
- `dev/initial_mixed_detection` : Initial detection of SIR2 subfamily and SIRim proteins (data not shown)
- `dev/minimal_alignment` : Alignment of a subsample of SIR2 and SIRim subfamily proteins (Figure S2)
- `dev/sirim/1_detection` : Detection of SIRim subfamily proteins (Used in all phylogenetic analyses)
- `dev/sirim/2_align` : Alignment of SIRim subfamily proteins
- `dev/sirim/3_phylo_sirim_domain`: Phylogeny of SIRim subfamily proteins (Figure 4a)
- `dev/sirim/3.1_additional_trees`: Phylogeny of SIRim subfamily proteins (Figure S7)
- `dev/sirim/3.2_phylo_full_protein`: Phylogeny of SIRim subfamily proteins
- `dev/sirim/3.3_phylo_euk`: Phylogeny of SIRim subfamily proteins (Figure S6, S8)
- `dev/sirim/4_defense_systems`: Detection of which of the SIR2 and SIRim subfamily proteins are involved in known defense systems (DefenseFinder) / are in genomic contexts enriched in defense systems (Figure S1)
- `dev/sirim/5_focus_eukaryotes` : Analyses regaring clades of eukaryotic SIRim (Figure 4, Figures S6-S8)
- `dev/sirtuins/1_detection` : Detection of SIR2 subfamily proteins (Used in all phylogenetic analyses)
- `dev/phylo_sir2_sirim` : Joint phylogeny of SIR2 and SIRim subfamily proteins (Figure 1a)

